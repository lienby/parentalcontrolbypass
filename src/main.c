#include <psp2/power.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/kernel/threadmgr.h>
#include <psp2/appmgr.h>
#include <psp2/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sceRegMgrSetKeyInt(const char*, const char*, int);
int sceRegMgrSetKeyStr(const char*, const char*, const char*, int);

int executeUriExit(char *uritext) {
	while(1) {
		sceAppMgrLaunchAppByUri(0x20000, uritext);
	}
}

int main(int argc, const char *argv[]) {
	sceRegMgrSetKeyStr("/CONFIG/SECURITY/PARENTAL", "passcode", (char[]){0x0}, 1);
	executeUriExit("psgm:play?titleid=NPXS10094");
	sceKernelExitProcess(0);
	return 0;
}
