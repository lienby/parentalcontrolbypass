When you run the app it will prompt you to set a new parental control passcode without needing to know the old one.

--Technical Details--
Parental Control settings are stored in registry.
/CONFIG/SECURITY/PARENTAL
the main one where after is the "passcode" key
This is actually a string. Despite only numbers being used
And if the string is immediately terminated (0x00 is terminator on PSV)
Then there is "no passcode" so this app just sets it to 0x00 then runs the app

Interestingly it seems you need to run the app twice in some cases for it to actually
ask you to set a new passcode. Unsure what causes this..

VPK: https://bitbucket.org/SilicaAndPina/parentalcontrolbypass/downloads/Parental_Control_Bypass.vpk  